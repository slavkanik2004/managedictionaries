public abstract class AbstractRegexCheck {
    String KeyRegex;
    String ValueRegex;

    public AbstractRegexCheck() {
        this.KeyRegex = "";
        this.ValueRegex = "";
    }

    public String getKey_regex(){return KeyRegex;}
    public String getValue_regex(){return ValueRegex;}
}
