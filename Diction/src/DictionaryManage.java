import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.io.*;
import java.lang.reflect.Array;
import java.util.*;
import java.util.regex.Pattern;



public class DictionaryManage {
    // добавить ввод пути для словаря
    private static ArrayList<LangDict> dicts;
    private static LangDict CurrentDict;
    private static String nameFileCurrentDict;
    private static ArrayList<String> namesFile;
    private static AbstractRegexCheck[] Regexs = new AbstractRegexCheck[]{ new LatinRusValues(), new LatinDigitValues()};
    private static int counter = 0;


    public DictionaryManage(String Path, typeDict type)
    {
        namesFile = new ArrayList<String>();
        dicts = new ArrayList<LangDict>();
        AddDict(Path, type);
        nameFileCurrentDict = namesFile.get(0);
        CurrentDict = dicts.get(0);


    }
    public static void AddDict(String Path, typeDict type)
    {
        try
        {
            Gson json = new Gson();
            FileReader reader = new FileReader(Path);
            namesFile.add(Path);
            if (namesFile.isEmpty()) return;
            dicts.add(json.fromJson(reader, LangDict.class));
            switch (type)
            {
                case EN_RU_TYPE:
                {
                    dicts.set(counter, dicts.get(counter).setRegex(Regexs[0]));
                    counter += 1;
                    break;
                }
                case EN_DIGIT_TYPE:
                {
                    dicts.set(counter, dicts.get(counter).setRegex(Regexs[1]));
                    counter += 1;
                    break;
                }
                default:
                {
                    System.out.println("Данного типа не существует");
                }
            }

        }
        catch (JsonSyntaxException e) {
            System.out.println("Неверный json файл. Проверьте верен ли синтаксис в json файлах");
        } catch (FileNotFoundException e) {
            System.out.println("Файл указанный в файле names.json не найден");
        }
    }
    public static void ChangeCurrentDict(String nameDict)
    {
        for (int i = 0; i < dicts.size(); i++)
        {
            if (dicts.get(i).getName().toLowerCase().replace(" ","").equals(nameDict.toLowerCase().replace(" ","")))
            {
                CurrentDict = dicts.get(i);
                nameFileCurrentDict = namesFile.get(i);
            }
        }
    }
    public static boolean[] AddElement(String key, String word) throws IOException {
        String reg = CurrentDict.getKey_regex();
        if (Pattern.matches(CurrentDict.getKey_regex(), key) && Pattern.matches(CurrentDict.getValue_regex(), word))
        {
            Map<String, String> UpdatedDict = CurrentDict.getWords();
            UpdatedDict.put(key, word);
            CurrentDict.UpdateWords(UpdatedDict);
            serialization(nameFileCurrentDict, CurrentDict);
            return new boolean[] {true, true};
        }
        return new boolean[]{Pattern.matches(CurrentDict.getKey_regex(), key), Pattern.matches(CurrentDict.getValue_regex(), word)};
    }
    public static String OutputDict()
    {
        Map<String,String> Dict = CurrentDict.getWords();
        String StringDict = "";
        for (Map.Entry<String, String> entry : Dict.entrySet())
        {
            StringDict += entry.getKey() + ": " + entry.getValue() + "\n";
        }
        return StringDict;
    }
    public static String TranslateWord(String key)
    {
        return CurrentDict.getWords().get(key) == null? "Такого слова нет": CurrentDict.getWords().get(key);
    }
    public String GetName()
    {
        return CurrentDict.getName();
    }
    public static ArrayList<String> GetAllNamesDict()
    {
        ArrayList<String> namesDicts = new ArrayList<String>();
        for (int i = 0; i < dicts.size(); i++)
        {
            namesDicts.add(dicts.get(i).getName());
        }
        return namesDicts;
    }
    public boolean DelElement(String key) throws IOException {
        if (Pattern.matches(CurrentDict.getKey_regex(), key))
        {
            Map<String, String> UpdatedDict = CurrentDict.getWords();
            UpdatedDict.remove(key);
            CurrentDict.UpdateWords(UpdatedDict);
            return serialization(nameFileCurrentDict, CurrentDict);
        }
        return false;
    }
    private static boolean serialization(String Path, LangDict Dict) throws IOException {
        File f = new File(Path);
        if (f.exists())
        {
            Gson json = new Gson();
            String writing = json.toJson(Dict, LangDict.class);
            FileWriter FW = new FileWriter(f.getAbsolutePath());
            FW.write(writing);
            FW.close();
            return true;
        }
        return false;
    }
}
