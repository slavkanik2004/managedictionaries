import java.util.Map;

public class LangDict{
    private String name;
    private Map<String, String> words;

    public transient AbstractRegexCheck regex;

    public LangDict(String Name, Map<String, String> Words)
    {
        this.name = Name;
        this.words = Words;
    }
    public String getName(){ return name;}
    public Map<String, String> getWords(){return words;}
    public void UpdateWords(Map<String, String> NewWords) { words = NewWords;}

    public int getLenDict(){return words.size();}
    public String getKey_regex()
    {
        return regex.KeyRegex;
    }
    public String getValue_regex()
    {
        return regex.ValueRegex;
    }
    @Override
    public String toString(){return name + " words_count: " + words.size();}
    public LangDict setRegex(AbstractRegexCheck reg)
    {
        regex = reg;
        return this;
    }

}
