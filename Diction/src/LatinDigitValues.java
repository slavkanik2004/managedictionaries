public class LatinDigitValues extends AbstractRegexCheck{
    public LatinDigitValues() {
        super();
        this.KeyRegex = "\\b\\w{4}\\b";
        this.ValueRegex = "\\b\\d{5}\\b";
    }
}
