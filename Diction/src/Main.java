import org.w3c.dom.ranges.RangeException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Dictionary;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

public class Main {
    public static DictionaryManage AddDict(String Path, DictionaryManage dict, int a)
    {
        try
        {
            File f = new File(Path);
            if (f.exists())
            {
                String name = f.getName();


                // тут где то ошибка
                if (Pattern.matches("^\\w+\\.json$", name))
                {
                    if (dict != null)
                    {
                        switch (a)
                        {
                            case 0:
                            {
                                dict.AddDict(Path, typeDict.EN_RU_TYPE);
                                return dict;
                            }
                            case 1:
                            {
                                dict.AddDict(Path, typeDict.EN_DIGIT_TYPE);
                                return dict;
                            }
                            default:
                            {
                                System.out.println("Введите существующий номер типа. #1");
                            }
                        }

                        return dict;
                    }
                    switch (a)
                    {
                        case 0:
                        {
                            DictionaryManage Dict = new DictionaryManage(Path, typeDict.EN_RU_TYPE);
                            return Dict;
                        }
                        case 1:
                        {
                            DictionaryManage Dict = new DictionaryManage(Path, typeDict.EN_DIGIT_TYPE);
                            return Dict;
                        }
                        default:
                        {
                            System.out.println("Введите существующий номер типа. #2");
                            return null;
                        }
                    }

                }
                return null;
            }
            return null;
        }
        catch (InputMismatchException e)
        {
            System.out.println("Введите существующий номер типа.");
        }
        return null;
    }

    public static DictionaryManage Dict;
    public static void main(String[] args) throws InterruptedException {

        while (true)
        {
            System.out.println("Введите номер типа словаря: ");
            System.out.println("0. Англо-русский словарь");
            System.out.println("1. Латино-цифровой словарь");
            int au = new Scanner(System.in).nextInt();

            System.out.println("Введите путь до словаря: ");

            Scanner in = new Scanner(System.in);

            Dict = AddDict(in.nextLine(), null, au);
            if (Dict != null) break;
            else
            {
                System.out.println("Путь до файла неверный или расширение файла не .json");
            }

        }

        while (true)
        {
            try
            {
                System.out.println(String.format("\nТекущий словарь: %s\n\n", Dict.GetName()));

                System.out.println("1. Добавить слово в словарь");
                System.out.println("2. Перевести слово");
                System.out.println("3. Сменить словарь");
                System.out.println("4. Удалить запись из словаря(по ключу)");
                System.out.println("5. Вывести словарь");
                System.out.println("6. Добавить словарь");


                Scanner in = new Scanner(System.in);
                int num = in.nextInt();


                System.out.println(String.format("Выбран пункт: %s",num));
                switch (num)
                {
                    case 1:
                    {
                        System.out.println("Введите слово, которое хотите добавить: ");
                        Scanner n = new Scanner(System.in);
                        String key = n.next();
                        String word = new Scanner(System.in).next();
                        if (!DictionaryManage.AddElement(key, word)[0])
                        {
                            System.out.println("Слово не соответствует условиям для добавления");
                            break;
                        }
                        if (!DictionaryManage.AddElement(key, word)[1])
                        {
                            System.out.println("Перевод не соответствует условиям для добавления");
                            break;
                        }
                        System.out.println("Слово успешно добавлено!");
                        System.out.println(DictionaryManage.OutputDict());
                        break;
                    }
                    case 2:
                    {
                        System.out.println(DictionaryManage.TranslateWord(new Scanner(System.in).next()));
                        break;
                    }
                    case 3:
                    {

                        System.out.println("Введите номер словаря, который хотите получить: ");
                        ArrayList<String> names = DictionaryManage.GetAllNamesDict();
                        for (int i = 0; i < names.size(); i++)
                        {
                            System.out.println(i + ". " + names.get(i));
                        }
                        int numb = Integer.parseInt(new Scanner(System.in).next());
                        Dict.ChangeCurrentDict(names.get(numb));
                        break;
                    }
                    case 4:
                    {


                        System.out.println("Введите слово, которое хотите удалить из словаря(ключ)");
                        String key = new Scanner(System.in).next();
                        if (Dict.DelElement(key)) System.out.println("Слово удалено.");
                        else System.out.println("В словаре нет указанного слова.");

                        break;
                    }
                    case 5:
                    {
                        System.out.println(Dict.OutputDict());
                        break;
                    }
                    case 6:
                    {
                        System.out.println("Введите номер типа словаря: ");
                        System.out.println("0. Англо-русский словарь");
                        System.out.println("1. Латино-цифровой словарь");
                        int au = new Scanner(System.in).nextInt();

                        System.out.println("Введите путь до словаря:");
                        Scanner inp = new Scanner(System.in);

                        DictionaryManage dt = AddDict(inp.nextLine(), Dict, au);
                        if (dt != null)
                        {
                            Dict = dt;
                            break;
                        }
                        else
                        {
                            System.out.println("Путь до файла неверный или расширение файла не .json");
                        }

                        break;
                    }
                    default:
                    {
                        System.out.println("Такого пункта не существует.");
                        break;
                    }
                }
            }
            catch (InputMismatchException e)
            {
                System.out.println("Введите существующий пункт меню.");
            }
            catch (RangeException e)
            {
                System.out.println("Введите число в диапазоне типа int");
            }
            catch (IllegalArgumentException e)
            {
                System.out.println(e.getMessage());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            catch (IndexOutOfBoundsException e)
            {
                System.out.println("Такого номера не существует.");
            }

            TimeUnit.SECONDS.sleep(1);



        }
    }
}