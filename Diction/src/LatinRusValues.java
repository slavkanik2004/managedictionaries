import java.time.temporal.ValueRange;

public class LatinRusValues extends AbstractRegexCheck{

    public LatinRusValues() {
        super();
        this.KeyRegex = "\\b[a-zA-Z]+\\b";
        this.ValueRegex = "\\b[а-яА-Я]+\\b";


    }
}
